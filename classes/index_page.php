<?php declare(strict_types=1);

class wc_index extends wc_default { // skeleton template

    private $page_text;
    private $characters;
    private $has_characters;

    public function render_head() { // generic head render function
        parent::render_head(); // inherit wc_default
    }

    public function render_body(){
        // TODO
        echo '<h1>'.$this->page_title.'</h1>
        ';
        echo '<h2>Characters</h2>
        ';
        if ($this->has_characters){
            echo '<ul>
            ';
            foreach ($this->characters as $character){
                echo '<li>'.'<a href="character/'.$character['id'].'">'.$character['name'].'</a>'.'</li>
                ';
            }
            echo '</ul>
            ';
        } else {
            echo '<p>No characters found</p>
            ';
        }
    }

    function __construct(Array $arguments){
        parent::__construct($arguments); // inherit wc_default

        $pdo = $arguments['pdo'];

        $sql = 'Select * From `characters`';
        $query = $pdo->prepare($sql);     // prepare query
        $query->execute(); // perform query
        $this->characters = $query->fetchall();     // get whatever it is that was queried

        $this->has_characters = (count($this->characters) != 0);
    }
}

?>
