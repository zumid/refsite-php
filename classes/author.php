<?php declare(strict_types=1);

class wc_author extends wc_default {

    private $author;

    public function render_head() {
        if($this->is_author){ // author found
            $this->page_title = $this->author['name'];
        } else {    // author not found
            $this->page_title = 'author not found!';
        }
        parent::render_head(); // inherit wc_default
    }

    private function show_author(Array $author){
        // define strings
        $author_strings = [
                          'Name' => $author['name'],
                          'Age' => $author['age'],
                          'Gender' => $author['gender']
                         ];
        // author name
        echo '<h1>' . $author['name'] . '</h1>
         ';
        echo '<h2>Data</h2>
         ';

        // data table
        echo '<table border=1>';
            foreach ($author_strings as $label => $value){
            if (!(is_null($value))) {
                echo '
           <tr>
             ';
                echo '<th>'. $label . '</th>
             ';
                echo '<td>' . $value . '</td>';
                echo '
           </tr>';
            }
            }
        echo '
        </table>
        ';

        echo '<h2>Basic Bio</h2>
        ';

        // bio
        echo '<p>' . $author['bio'] . '</p>
        ';
    }

    private function edit_author(Array $author){
        // TODO
        echo '<h1>Edit function</h1>';
        echo '<p>Coming soon!</p>
             ';
    }

    private function new_author(){
        // TODO
        echo '<h1>New function</h1>';
        echo '<p>Coming soon!</p>
             ';
    }

    private function author_not_found(){
        // TODO
        echo '<h1>Not found</h1>';
        echo '<p>The specified author was not found.</p>
        ';
    }

    public function render_body(){
        if( isset($_GET['action']) ){ // has action
            if($this->is_author){ // author found
                switch($_GET['action']){
                    case 'edit':
                        $this->edit_author($this->author);
                        break;
                    case 'show':
                    default:
                        $this->show_author($this->author);
                        break;
                }
            } else { // author not found
                switch($_GET['action']){
                    case 'new':
                        $this->new_author();
                        break;
                    default:
                        $this->author_not_found();
                        break;
                }
            }
        } else {  // has no action
            if($this->is_author){ // author found
                $this->show_author($this->author);
            } else { // author not found
                $this->author_not_found();
            }
        }
    }

    public function __construct(Array $arguments){
        parent::__construct($arguments); // inherit wc_default

        $id = $arguments['uri'][1];
        $pdo = $arguments['pdo'];

        $sql = 'Select * From `authors` Where `id` = ?';
        $query = $pdo->prepare($sql);     // prepare query
        $query->execute([$id]); // perform query
        $this->author = $query->fetch();     // get whatever it is that was queried

        $this->is_author = is_array($this->author);
    }
}

?>
