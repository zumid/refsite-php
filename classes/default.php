<?php declare(strict_types=1);

class wc_default { // skeleton template

    public $page_title;

    public function render_head() { // generic head render function
        echo '<title>'.$this->page_title.'</title>
        ';
        echo '<meta charset="utf-8">
        ';
        echo '<meta name="viewport" content="width=device-width,initial-scale=1">
        ';
        echo '<base href="'.BASE_URL.'">
        ';
        echo '<link rel="stylesheet" href="assets/element.css">
        ';
    }

    public function render_body(){
    }

    public function __construct(Array $arguments){
        $this->page_title = $arguments['title'];
    }
}

?>
