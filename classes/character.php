<?php declare(strict_types=1);

class wc_character extends wc_default {

    private $character;

    public function render_head() {
        if($this->is_character){ // character found
            $this->page_title = $this->character['name'];
        } else {    // character not found
            $this->page_title = 'Character not found!';
        }
        parent::render_head(); // inherit wc_default
    }

    private function show_character(Array $character){
        // define strings
        $character_strings = [
                          'Full Name' => $character['name'],
                          'Nickname' => $character['nick'],
                          'Species' => $character['species'],
                          'Gender' => $character['gender'],
                          'Age' => $character['age'],
                          'Height' => $character['height'],
                          'Weight' => $character['weight']
                         ];
        // character name
        echo '<h1>' . $character['name'] . '</h1>
         ';
        echo '<h2>Stats</h2>
         ';

        // stats table
        echo '<table border=1>';
            foreach ($character_strings as $label => $value){
            if (!(is_null($value))) {
                echo '
           <tr>
             ';
                echo '<th>'. $label . '</th>
             ';
                echo '<td>' . $value . '</td>';
                echo '
           </tr>';
            }
            }
        echo '
        </table>
        ';

        echo '<h2>Basic Bio</h2>
        ';

        // bio
        echo '<p>' . $character['bio'] . '</p>
        ';
    }

    private function edit_character(Array $character){
        // TODO
        echo '<h1>Edit function</h1>';
        echo '<p>Coming soon!</p>
             ';
    }

    private function new_character(){
        // TODO
        echo '<h1>New function</h1>';
        echo '<p>Coming soon!</p>
             ';
    }

    private function character_not_found(){
        // TODO
        echo '<h1>Not found</h1>';
        echo '<p>The specified character was not found.</p>
        ';
    }

    public function render_body(){
        if( isset($_GET['action']) ){ // has action
            if($this->is_character){ // character found
                switch($_GET['action']){
                    case 'edit':
                        $this->edit_character($this->character);
                        break;
                    case 'show':
                    default:
                        $this->show_character($this->character);
                        break;
                }
            } else { // character not found
                switch($_GET['action']){
                    case 'new':
                        $this->new_character();
                        break;
                    default:
                        $this->character_not_found();
                        break;
                }
            }
        } else {  // has no action
            if($this->is_character){ // character found
                $this->show_character($this->character);
            } else { // character not found
                $this->character_not_found();
            }
        }
    }

    public function __construct(Array $arguments){
        parent::__construct($arguments); // inherit wc_default

        $id = $arguments['uri'][1];
        $pdo = $arguments['pdo'];

        $sql = 'Select * From `characters` Where `id` = ?';
        $query = $pdo->prepare($sql);     // prepare query
        $query->execute([$id]); // perform query
        $this->character = $query->fetch();     // get whatever it is that was queried

        $this->is_character = is_array($this->character);
    }
}

?>
