<?php declare(strict_types=1);

// base URL
define('BASE_URL', 'https://localhost/');

ob_start();        // start object buffer
session_start();    // start session cookie

define('DEBUG',1);    // to print extra debug stuff

// Debug
if(defined("DEBUG")){
    ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);
}

// Database
$pdo_host = 'localhost';
$pdo_db   = 'db';
$pdo_user = 'user';
$pdo_pass = 'pass';

$pdo_connection = "mysql:host=$pdo_host;dbname=$pdo_db";
$pdo_options = [
    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION
];

try {
     $pdo = new PDO($pdo_connection, $pdo_user, $pdo_pass, $pdo_options);
} catch (\PDOException $e) {
     echo $e->getMessage() . (int)$e->getCode();
}

?>
