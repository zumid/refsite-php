<?php declare(strict_types=1);

    if (!(isset($page))){
        // can't find the page class
        http_response_code(400);
        die('400 - Bad Request');

    } else {    // render the page
    echo '<!DOCTYPE html>
    ';

    // start
    echo '<html class="element">
    ';

    echo '<head>
    ';
        $page->render_head();
    echo '</head>
    ';

    echo '<body>
        ';
        $page->render_body();
    echo '</body>
    ';

    echo '</html>'; // end
    }
?>
