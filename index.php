<?php declare(strict_types=1);

    include_once('include/config.php');

    // get url
    $request = $_SERVER['REQUEST_URI'];
    $request = strtok($request, '?');    // strip all parameters

    // setup class prefix
    $class_name = 'wc_';

    // setup
    $arguments = [
                     'uri' => NULL,
                     'title' => 'Index page',
                     'pdo' => $pdo,
                 ];

    // really basic routing with regular expressions (regex)
    // will match character/<id> or author/<id>
    // where the ID is any number
    $id_match = '/(character|author)\/[0-9]+$/';

    if( preg_match($id_match, $request, $match) ){
        $uri = explode('/',reset($match));    // array: ['character', 1]

        $sub    = reset($uri);    // first array element = subpage
        $sub_id = next($uri);     // second array element = ID of subpage

        $class_name .= $sub; // generic class name, appended to class_name
        $class_file = $sub; // which file to include
    }
    else {
        $uri = explode('/',$request);
        $class_name .= 'index';
        $class_file = 'index_page';
    }

    $arguments['uri'] = $uri;

    if( file_exists('classes/'.$class_file.'.php') ){
        include_once('classes/default.php');            // base page template
        include_once('classes/'.$class_file.'.php');    // custom page template
        $page = new $class_name($arguments);
    }
    include_once('include/render.php')
?>
