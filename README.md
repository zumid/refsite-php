# Refsite

## What

PHP 7. See [here](https://sarajyeo.xyz/priv/refsite_tutorial/mdwiki/).

Use [Element.css](https://elementcss.neocities.org/).

## SQL Setup

```
CREATE TABLE `characters` (
  `id` int(11) NOT NULL,
  `author` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `nick` varchar(255) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `species` varchar(255) DEFAULT NULL,
  `height` double DEFAULT NULL,
  `weight` double DEFAULT NULL,
  `bio` text DEFAULT NULL
) 
```

```
CREATE TABLE `authors` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `age` int(11) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `bio` text DEFAULT NULL
)
```

## Config

Edit include/config.php
